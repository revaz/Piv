import yaml

data = []

album = {"album":"Noel a Sion"}
data.append(album)


photo = {}
photo["name"] = "DSC_7424.JPG"
photo["label"] = None
photo["angle"] = 0.0
photo["x"] = 0
photo["y"] = 0
data.append({"photo":photo})


photo = {}
photo["name"] = "DSC_7425.JPG"
photo["label"] = "a label"
photo["label"] = """
    Photo Name: DSC_7592
    Camera: NIKON CORPORATION NIKON Z 6_2
    Date: 2022:12:25 18:06:32
    ImageSize: 6048 X 4024
    Exposure Time: 1/59 sec sec
    Exposure Program: Manual
    Aperture: f/6.3
    FocalLength: 200 mm
    Flash: fill fired (!)
    Metering Mode: 3
    ISOSpeedRatings: 200
    LightSource: 0
    Compression: 6
    Orientation: 1
    ResolutionUnit: Inch
    XResolution: 300
    YResolution: 300
    YCbCrPositioning: 2
    Software: Ver.01.50
"""
data.append({"photo":photo})

photo = {}
photo["name"] = "DSC_7426.JPG"
photo["label"] = "a label"
photo["video"] = "./DSC_7505.MOV"
data.append({"photo":photo})






with open("qq.yaml", mode="wt", encoding="utf-8") as yamlfile:
  yaml.dump(data,yamlfile)



