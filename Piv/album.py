
from PyQt5 import QtGui, QtCore, QtWidgets


import os,sys
import string
import shutil

from . import param
import exiftool


#import Image
from PIL import Image

def parse(text):

    out_text = []
    text = str.strip(text)
    c1 = 0
    c2=str.find(text,'\n')
    while c2 != -1:
      out_text.append(text[0:c2])
      c1 = c2+1
      text = text[c1:]
      c2=str.find(text,'\n')

    out_text.append(text)

    return out_text






FLEXSLIDERDIR="/photos/lib/FlexSlider"
HOVERBOXDIR="/photos/lib/hoverbox"



##########################
# flexslider
##########################



flex_html_header="""

<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta content="charset=utf-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">

  <!-- Demo CSS -->
    <link rel="stylesheet" href="%s/demo/css/demo.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="%s/flexslider.css" type="text/css" media="screen" />

    <!-- Modernizr -->
  <script src="%s/demo/js/modernizr.js"></script>

</head>





<body class="loading">


  <div class="flexslider">
    <ul class="slides">

"""%(FLEXSLIDERDIR,FLEXSLIDERDIR,FLEXSLIDERDIR)




flex_html_footer="""



    </ul>
  </div>



  <!-- jQuery -->
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="%s/demo/js/libs/jquery-1.7.min.js">\x3C/script>')</script>



  <!-- FlexSlider -->
  <script defer src="%s/jquery.flexslider.js"></script>

  <script type="text/javascript">


    $(window).load(function(){
      $('.flexslider').flexslider({
        animation: "slide",

        start: function(slider){
          $('body').removeClass('loading');
        }

      });
    });


  </script>






</body>
</html>


"""%(FLEXSLIDERDIR,FLEXSLIDERDIR)





##########################
# hoverbox
##########################





def hoverbox_html_header(title="my title"):



  txt="""

  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <title>Hoverbox Image Gallery</title>
  <link rel="stylesheet" href='%s/css/hoverbox.css' type="text/css" media="screen, projection" />
  <!--[if lte IE 7]>
  <link rel="stylesheet" href='%s/css/ie_fixes.css' type="text/css" media="screen, projection" />
  <![endif]-->
  </head>
  <body>
  <h1>%s</h1>
  <ul class="hoverbox">
  \n

  """%(HOVERBOXDIR,HOVERBOXDIR,title)

  return txt




def hoverbox_html_footer():

  txt="""
  </ul>


  <p><a href="slider.html">SLIDER</a> </p>


  </body>
  </html>
  \n
  """

  return txt


def hoverbox_html_image_link(filename=None):

  txt="""
  <li><a href="#"><img src="%s" alt="description" /><img src="%s" alt="description" class="preview" /></a></li>
   \n
  """%(filename,filename)

  return txt














class Album:

  ##########################
  def __init__(self,filename=None,photos=None,name=None,basedir='.'):
  ##########################

    self.keywords = {                                       \
      'begin'	: {	'image'		:self.AddPhoto,		\
                  'header'	:self.AddHeader,		\
                  'label'		:self.AddLabel,
                  'video'		:self.AddVideo,
                  'items'         :self.AddItem},           \
      'end'	: {	  'image'		:self.DoNothing,        \
                  'header'	:self.DoNothing,        \
                  'label'		:self.DoNothing,
                  'video'		:self.DoNothing,
                  'items'             :self.DoNothing},		\
      'param': {  'nickname'	:self.AddNickname,		\
                  'album'             :self.AddAlbum,		\
                  'angle'             :self.AddAngle,		\
                  'x'                 :self.AddX,			\
                  'y'                 :self.AddY,			\
                  'size'              :self.AddSize},
      'album'	: { 'name'          :self.AlbumName} }




    self.HEADER_FORMAT = """
    Photo Name: %(PhotoName)s
    Camera: %(Make)s %(Model)s
    Date: %(DateTime)s
    ImageSize: %(ImageWidth)s X %(ImageHeight)s
    Exposure Time: %(ExposureTime)s sec
    Exposure Program: %(ExposureProgram)s
    Aperture: f/%(FNumber)s
    FocalLength: %(FocalLength)s mm
    Flash: %(Flash)s
    Metering Mode: %(MeteringMode)s
    ISOSpeedRatings: %(ISOSpeedRatings)s
    LightSource: %(LightSource)s
    Compression: %(Compression)s
    Orientation: %(Orientation)s
    ResolutionUnit: %(ResolutionUnit)s
    XResolution: %(XResolution)s
    YResolution: %(YResolution)s
    YCbCrPositioning: %(YCbCrPositioning)s
    Software: %(Software)s
    """

    self.ProgramModeSwitcher = {
        0:	'Unidentified',
        1:	'Manual',
        2:	'Program Normal',
        3:	'Aperture Priority',
        4:	'Shutter Priority',
        5:	'Program Creative',
        6:	'Program Action',
        7:	'Portrait Mode',
        8:	'Landscape Mode',
        }

    self.FlashSwitcher = {
        0:	'no',
        1:	'fired',
        5:	'fired (?)',
        7:	'fired (!)',
        9:	'fill fired',
        13:	'fill fired (?)',
        15:	'fill fired (!)',
        16:	'off',
        24:	'auto off',
        25:	'auto fired',
        29:	'auto fired (?)',
        31:	'auto fired (!)',
        32:	'not available'
        }

    self.ResolutionUnitSwitcher = {
        1:	'Not Absoulute',
        2:	'Inch',
        3:	'Centimeter'
        }

    self.dir      = basedir
    self.filename = filename
    self.name     = name

    self.photos   = []
    self.date     = None

    self.i        = 0
    self.imaster  = 0


    self.et = exiftool.ExifTool()
    self.et.start()


    if filename != None:
      self.filename = os.path.basename(filename)
      self.dir =  os.path.dirname(filename)
    else:
      self.dir = basedir


    # read the album
    if self.filename != None:
      self.Read()



    # add other photos
    if photos!=None:
      self.AddPhotos(photos)


    print("---------- list of images in album ------------")
    print((self.List()))
    print("-----------------------------------------------")


  ##########################
  def Info(self):
  ##########################

    self.ListAll()




  ##########################
  def AddPhotos(self,photos):
  ##########################

    for photo in photos:
      self.AddPhoto(photo)


  ##########################
  def AddPhoto(self,file):
  ##########################


    if self.dir==None:
      self.dir = '.'

    file = os.path.join(self.dir,file)

    if os.path.exists(file):
    
      video = None
      # check if it is an image or a movie
      f,ext = os.path.splitext(file)
      bname = os.path.basename(f)
      if ext == ".MOV" or ext == ".mov":
        # a movie : create an image out if it
        
        video = file
        
        import cv2
        # Opens the Video file
        cap= cv2.VideoCapture(file)
                
        # rename
        #file = os.path.join(self.dir,"%s.png"%(bname))
        file = os.path.join("%s.png"%(file))
        
        # read one frame
        ret, frame = cap.read()
        cv2.imwrite(file,frame)
        cap.release()
        cv2.destroyAllWindows()
        
        
        # read the photo date/time
        metadata = self.et.get_metadata(video)
        tags = {}
        dir_name, file_name       = os.path.split(file)
        photo_name, ext           = os.path.splitext(file_name)
        tags["PhotoName"]         = photo_name
        tags["DateTime"]          =  metadata["MakerNotes:NikonDateTime"]
        tags["Make"]              =  metadata["MakerNotes:Make"]
        tags["Model"]             =  metadata["MakerNotes:Model"]
        tags["ImageWidth"]        =  metadata["QuickTime:ImageWidth"]
        tags["ImageHeight"]       =  metadata["QuickTime:ImageHeight"]
        
        
        
      newphot = Photo(photo_name=file,directory=self.dir,number=len(self.photos)+1,album=self.filename,video=video)
      self.photos.append(newphot)


      print("AddPhoto: %s"%file)

      # read the tags
      photo =  self.photos[-1]
      
      
      if video:
        # add the tags that has been taken from the video
        photo.add_tags(tags)
        
      else:
        # read it 
       

        try:
          # old exiftool
          #tags = exiftool.get_tags(photo.name)
        
          #self.et.start()  # done before
          metadata = self.et.get_metadata(photo.name)
          #self.et.terminate()  # never terminated...
        
          tags = {}
          dir_name, file_name       = os.path.split(photo.name)
          photo_name, ext           = os.path.splitext(file_name)
          tags["PhotoName"]         = photo_name
          tags["DateTime"]          =  metadata["EXIF:DateTimeOriginal"]
          tags["Make"]              =  metadata["EXIF:Make"]
          tags["Model"]             =  metadata["EXIF:Model"]
          tags["ImageWidth"]        =  metadata["EXIF:ExifImageWidth"]
          tags["ImageHeight"]       =  metadata["EXIF:ExifImageHeight"]
        
          exptime = float(metadata["EXIF:ExposureTime"])
          if exptime < 1:
           exptime = int(1/exptime)
           tags["ExposureTime"]     =  "1/%s sec"%exptime
          else:
           tags["ExposureTime"]     =  "%s sec"%exptime
        
        
          ExposureProgram = metadata["EXIF:ExposureProgram"]
        
          tags["ExposureProgram"] = self.ProgramModeSwitcher.get(ExposureProgram)
          tags["FNumber"]         = "%s"%metadata["Composite:Aperture"]
          tags["FocalLength"]     = "%s"%metadata["EXIF:FocalLength"]
        
          tags["Flash"]           = "%s"%self.FlashSwitcher.get(metadata["EXIF:Flash"])
        
          tags["MeteringMode"]   = "%s"%metadata["EXIF:MeteringMode"]
          tags["ISOSpeedRatings"] = "%s"%metadata["EXIF:ISO"]
          tags["LightSource"]     = "%s"%metadata["EXIF:LightSource"]
          tags["Compression"]     = "%s"%metadata["EXIF:Compression"]
          tags["Orientation"]     = "%s"%metadata["EXIF:Orientation"]
          tags["Software"]        = "%s"%metadata["EXIF:Software"]
        
          tags["ResolutionUnit"]     = "%s"%self.ResolutionUnitSwitcher.get(metadata["EXIF:ResolutionUnit"])
          tags["XResolution"]        = "%s"%metadata["EXIF:XResolution"]
          tags["YResolution"]        = "%s"%metadata["EXIF:YResolution"]
          tags["YCbCrPositioning"]   = "%s"%metadata["EXIF:YCbCrPositioning"]
        
        
        except:
          #print("get_tags fail")
          tags = {}
        
        # add the tags
        photo.add_tags(tags)



      # add the header
      try:
        header =  self.HEADER_FORMAT % tags
        photo.add_header(header)
      except:
        #print("bad header format")
        pass

      # read angle if rotation is defined
      if 'Orientation' in photo.tags:
        if   photo.tags['Orientation']=='1':
          photo.add_angle(0)
        elif photo.tags['Orientation']=='6':
          photo.add_angle(-90)
        elif photo.tags['Orientation']=='8':
          photo.add_angle(90)
        else:
          pass



      # thumbnail
      idir  = 'Icons'
      lidir = os.path.join(self.dir,idir)
      thumbnail_name = os.path.join(lidir,os.path.basename(photo.name))

      if os.path.exists(thumbnail_name):
        photo.SetThumbnail(thumbnail_name)


    else:
      print(("warning : ",file," does not exit"))



  ##########################
  def List(self):
  ##########################

    for photo in self.photos:
      print((photo.name))


  ##########################
  def ListAll(self):
  ##########################

    for photo in self.photos:      
      fmt = "{0:06d} {1:20s} album={2:20s} date={3:10s} video={4:s}".format(photo.number,photo.name,str(photo.params.get('display','album')),str(photo.GetDate()),str(photo.video))
      print(fmt)
      
      


  ##########################
  def GetPhotosList(self):
  ##########################

    photos_list = []

    i = 0

    for photo in self.photos:

      photo_info = {}
      photo_info['number']  = photo.number
      photo_info['name']    = photo.name
      photo_info['album']	= photo.GetAlbum()
      photo_info['dat']		= photo.GetDate()
      photo_info['thumbnail']   = photo.GetThumbnail()

      photo_info['i']       = i


      photos_list.append(photo_info)

      i = i + 1


    return photos_list


  ##########################
  def CreateIcons(self):
  ##########################

    thumbnail_size = 256,256


    # create the directory
    idir  = 'Icons'
    lidir = os.path.join(self.dir,idir)



    # create icon directory         # here we should ask
    if os.path.exists(lidir):
      shutil.rmtree(lidir)


    os.mkdir(lidir)

    for photo in self.photos:

      thumbnail_name = os.path.join(lidir,os.path.basename(photo.name))
      print(thumbnail_name)

      image = Image.open(photo.name,)
      image.thumbnail(thumbnail_size, Image.ANTIALIAS)

      angle = photo.params.get('display','angle')
      if angle != 0:
        image = image.rotate(angle)


      image.save(thumbnail_name)

      photo.SetThumbnail(thumbnail_name)


    print("Done.")


  ##########################
  def Export(self,dest_directory):
  ##########################


     # add current directory name
    date = self.FindDate()
    dest_directory = os.path.join(dest_directory,"%s_%s"%(date,self.name))

    if os.path.isdir(dest_directory):
      pass
    else:
      os.mkdir(dest_directory)


    source = self.filename
    if os.path.isfile(source):
      dest = os.path.join(dest_directory,source)
      print(("copy %s to %s"%(source,dest)))
      shutil.copyfile(source,dest)


    '''
    source = "index.html"
    if os.path.isfile(source):
      dest = os.path.join(dest_directory,source)
      print "copy %s to %s"%(source,dest)
      shutil.copyfile(source,dest)
    '''

    source = "Icons"
    if os.path.isdir(source):
      dest = os.path.join(dest_directory,source)
      if os.path.isdir(dest):
        shutil.rmtree(dest)
      print(("copy %s to %s"%(source,dest)))
      shutil.copytree(source,dest)

    '''
    source = "images"
    if os.path.isdir(source):
      dest = os.path.join(dest_directory,source)
      if os.path.isdir(dest):
        shutil.rmtree(dest)
      print "copy %s to %s"%(source,dest)
      shutil.copytree(source,dest)

    source = "Pages"
    if os.path.isdir(source):
      dest = os.path.join(dest_directory,source)
      if os.path.isdir(dest):
        shutil.rmtree(dest)
      print "copy %s to %s"%(source,dest)
      shutil.copytree(source,dest)

    source = "subimages"
    if os.path.isdir(source):
      dest = os.path.join(dest_directory,source)
      if os.path.isdir(dest):
        shutil.rmtree(dest)
      print "copy %s to %s"%(source,dest)
      shutil.copytree(source,dest)
   '''

    # now, copy the photos
    #for photo in self.photos:
    #  source = photo.name
    #  if os.path.isfile(source):
    #	dest = os.path.join(dest_directory,photo.name)
    #	print "copy %s to %s"%(source,dest)
    #	shutil.copyfile(source,dest)

    # now, copy the photos (change size)
    for photo in self.photos:
      source = photo.name
      if os.path.isfile(source):
        dest = os.path.join(dest_directory,photo.name)

    f=2.
    image = Image.open(source)
    size = (int(image.size[0]/f),int(image.size[1]/f))

    print(("reducing %s %s to %s %s"%(source,image.size,dest,size)))

    image = image.resize(size)
    image.save(dest)




  ##########################
  def GetMaster(self):
  ##########################
    """
    return master image
    The master image is the image
    actually displayed or the first one of
    a serie (if nx!=0 and/or ny!=0).
    """
    return self.photos[self.imaster]


  ##########################
  def SetMaster(self,i):
  ##########################
    self.imaster = i

  ##########################
  def PrintMaster(self):
  ##########################
    print((os.getcwd()))
    print((self.photos[self.imaster].name))


  ##########################
  def LinkMaster(self,dirlink):
  ##########################
    base = os.getcwd()
    img  = self.photos[self.imaster].name
    name = os.path.basename(img)

    print((os.path.join(base,img),"-->",os.path.join(dirlink,name)))
    os.symlink(os.path.join(base,img),os.path.join(dirlink,name))



  ##########################
  def SetCurrentAsMaster(self):
  ##########################
    self.imaster = self.i


  ##########################
  def GetNext(self):
  ##########################
    self.i = self.i+1
    if self.i == len(self.photos):
      self.i=0

    return self.GetCurrent()

  ##########################
  def GetPrev(self):
  ##########################
    self.i = self.i-1
    if self.i == -1:
      self.i=len(self.photos)-1
    return self.GetCurrent()


  ##########################
  def GetSpec(self,i):
  ##########################
    self.i = i
    return self.GetCurrent()


  ##########################
  def GetCurrent(self):
  ##########################
    """
    The current image is the next
    one to be opened.
    """
    return self.photos[self.i]


  ##########################
  def GetPrevious(self):
  ##########################
    """
    like GetPrev, but do not change self.i
    """
    if self.i == -1:
      i=len(self.photos)-1
    else:
      i = self.i
        
    return self.photos[self.i-1]


  ##########################
  def GetCurrentAndMoveToNextOne(self):
  ##########################
    current = self.photos[self.i]
    self.i = self.i+1
    if self.i == len(self.photos):
      self.i=0

    return current


  ##########################
  def GetCurrentAndMoveToPreviousOne(self):
  ##########################

    current = self.photos[self.i]
    self.i = self.i-1
    if self.i == -1:
      self.i=len(self.photos)-1


    return current




  ##########################
  def RemoveCurrent(self):
  ##########################
    if len(self.photos)>0:
      del self.photos[self.i]

    print((self.List()))


  ##########################
  def RemoveMaster(self):
  ##########################
    if len(self.photos)>0:
      del self.photos[self.imaster]

    self.i = self.i-1

    print((self.List()))




  ##########################
  def AddHeader(self,value):
  ##########################
    
    header = ''
    line = self.f.readline()	             # read next line
    keyword,param,value = self.ExtractLine(line)     # extract info from line

    while (keyword,param) != ('end','header'):
      header=header+line
      line = self.f.readline()	             # read next line
      keyword,param,value = self.ExtractLine(line)   # extract info from line


    self.photos[len(self.photos)-1].add_header(header)



  ##########################
  def AddVideo(self,filename):
  ##########################

    video = self.f.readline()	             # read next line
    self.photos[len(self.photos)-1].add_video(video)



  ##########################
  def AddNickname(self,nickname):
  ##########################

    self.photos[len(self.photos)-1].add_nickname(nickname)

  ##########################
  def AddAlbum(self,album):
  ##########################

    self.photos[len(self.photos)-1].add_album(album)

  ##########################
  def AddLabel(self,value):
  ##########################

    label = ''
    line = self.f.readline()

    # read next line
    keyword,param,value = self.ExtractLine(line)     # extract info from line

    while (keyword,param) != ('end','label'):
      label=label+line
      line = self.f.readline()	             # read next line
      keyword,param,value = self.ExtractLine(line)   # extract info from line

    label=str.rstrip(label)				 # cut the trail
    self.photos[len(self.photos)-1].add_label(label)


  ##########################
  def AddItem(self,value):
  ##########################

    line = self.f.readline()	             # read next line
    keyword,param,value = self.ExtractLine(line)     # extract info from line

    while (keyword,param) != ('end','items'):

      try:
        sline = str.split(line,' ')
        n = len(sline[0]) + len(sline[1]) + len(sline[2]) + 3
        item = {'x':int(sline[1]),'y':int(sline[2]),'text':line[n:]}
        self.photos[len(self.photos)-1].add_item(item)
      except:
        print(("problem decoding line :",line))

      line = self.f.readline()	             # read next line
      keyword,param,value = self.ExtractLine(line)



  ##########################
  def AddAngle(self,value):
  ##########################

    angle = float(value)
    self.photos[len(self.photos)-1].add_angle(angle)

  ##########################
  def AddSize(self,value):
  ##########################

    size = float(value)
    self.photos[len(self.photos)-1].add_size(size)

  ##########################
  def AddX(self,value):
  ##########################

    x = int(value)
    self.photos[len(self.photos)-1].add_x(x)

  ##########################
  def AddY(self,value):
  ##########################

    y = int(value)
    self.photos[len(self.photos)-1].add_y(y)



  ##########################
  def DoNothing(self,value):
  ##########################
    pass




  ##########################
  def AlbumName(self,name):
  ##########################

    if name == 'None':
      name = None
    self.name = name





  ##########################
  def GetDate(self,photo):
  ##########################

    # first, look at the tags

    return photo.GetDate()



  ##########################
  def FindDate(self):
  ##########################

    date = None

    for photo in self.photos:
      date = self.GetDate(photo)
      if date != None:
        break

    return date




  ##########################
  def ExtractLine(self,line):
  ##########################

    # on determine le "keyword"
    c1=str.find(line,"\\")
    c2=str.find(line,"{")
    keyword = line[c1+1:c2]
    # on determine le "param"
    c1=str.find(line,"{")
    c2=str.find(line,"}")
    param = line[c1+1:c2]
    # on determine la "value"
    c1=str.find(line,"[")
    c2=str.find(line,"]")
    if c1 != -1:
      value = line[c1+1:c2]
    else:
      value = ""

    if keyword in self.keywords:
      if param in self.keywords[keyword]:
        self.keywords[keyword][param](value)          # on execute l'action associee

    return keyword,param,value





  ##########################
  def Read(self):
  ##########################

    line = " "
    self.f = open(os.path.join(self.dir,self.filename))
    while line != "":
      line = self.f.readline()		# read next line
      self.ExtractLine(line)		# extract info from line

    self.f.close()



  ##########################
  def Write(self,savename='album.piv'):
  ##########################
    #savename = "%s_%s.piv"%(self.date,self.name)

    if self.name == None:
      name,answer = QtWidgets.QInputDialog.getText(None, "Album name", "Enter the name of the album:")

      if   answer:
        self.name = name
      else:
        return




    if self.date == None:
      self.date = self.FindDate()
      if self.date == None:
        #self.date = askstring('album date','Please, enter the date of the album (aaaammdd)')
        print("WARNING : no date found")
        self.date = '00000000'



    f = open(savename,'w')
    f.write("\\album{name}[%s]\n"%(self.name))

    for photo in self.photos:

      # open image, image name
      f.write("\\begin{image}["+os.path.basename(photo.name)+"]\n")

      # write header
      if len(photo.header) > 0:
        f.write("\\begin{header}\n")
        for h in photo.header:
          f.write(h)
        f.write("\\end{header}\n")

      # write label
      if len(photo.label) > 0:
        f.write("\\begin{label}\n")
        for l in photo.label:
          f.write(l)
        f.write("\n\\end{label}\n")

      # write item
      if len(photo.items) > 0:
        f.write("\\begin{items}\n")
        for i in photo.items:
          f.write('text %d %d %s'%(i['x'],i['y'],i['text']))
        f.write("\\end{items}\n")

      # write label
      if photo.video is not None:
        f.write("\\begin{video}\n")
        f.write(photo.video)
        f.write("\n\\end{video}\n")


      # write options
      for param in photo.params.list_all():
        line = "\\param{%s}[%s]\n" % (param[1],param[2])
        f.write(line)


      # close image
      f.write("\\end{image}\n")
      f.write("\n")

    f.close()




  ##########################
  def WriteHtml(self):
  ##########################

#    html_name = os.path.join(self.dir,'index.html')
#    sdir =     os.path.join(self.dir,'subimages')
#    idir =     os.path.join(self.dir,'images')
#    pdir =     os.path.join(self.dir,'Pages')


    html_name = os.path.join(self.dir,'index.html')

    # small dir
    sdir =      'subimages'
    idir =      'images'
    pdir =      'Pages'

    # long dir
    lsdir =     os.path.join(self.dir,'subimages')
    lidir =     os.path.join(self.dir,'images')
    lpdir =     os.path.join(self.dir,'Pages')


    if self.name == None:
      name,answer = QtWidgets.QInputDialog.getText(None, "Album name", "Enter the name of the album:")

      if   answer:
        self.name = name
      else:
        return


    #savename = QtGui.QFileDialog.getSaveFileName(None,"Save File", os.path.basename(html_name))
    savename = os.path.basename(html_name)


    if savename != "":

      if os.path.exists(lidir):
        shutil.rmtree(lidir)
      if os.path.exists(lsdir):
        shutil.rmtree(lsdir)
      if os.path.exists(lpdir):
        shutil.rmtree(lpdir)




      os.mkdir(lsdir)
      os.mkdir(lpdir)
      os.mkdir(lidir)

      # open the html file

      f = open(html_name,'w')
      f.write("""<html>\n<center>\n""")


      f.write('<hr><p>\n')
      f.write('''<b><font color="#3366FF"><font size=+2>'''+self.name+'''</font></font></b>\n''')
      f.write('<p><hr>\n')

      f.write('<br><br>\n')

      current = 0
      # loop over all photos
      for photo in self.photos:

        fullphotoname = os.path.join(self.dir,os.path.basename(photo.name))
        angle = photo.params.get('display','angle')

        ################
        # rotate image
        ################
        if angle != 0:
          lnname = os.path.join(lidir,os.path.basename(photo.name))
          os.system("convert -rotate %i %s %s"%(-angle,str.replace(fullphotoname,' ','\ '),str.replace(lnname,' ','\ ')))
          print(("creating (1) :",str.replace(fullphotoname,' ','\ '),str.replace(lnname,' ','\ ')))
          lnname = os.path.join(idir,os.path.basename(photo.name))
        else:
          lnname = os.path.basename(photo.name)


        bname = "%0000006d"%(photo.number)
        nickname = photo.params.get('display','nickname')

        try:
          next = "%0000006d"%(self.photos[self.photos.index(photo)+1].number)
        except:
          next = None

        try:
          prev = "%0000006d"%(self.photos[self.photos.index(photo)-1].number)
        except:
          prev = None

        ################
        # make sub images
        ################
        sname = os.path.join(lsdir,os.path.basename(photo.name))
        print(("creating (2) :",str.replace(fullphotoname,' ','\ '),str.replace(sname,' ','\ ')))
        if angle != 0:
          cmd = "convert -size 80x60 -rotate %i %s %s"%(-angle,str.replace(fullphotoname,' ','\ '),str.replace(sname,' ','\ '))
        else:
          cmd = "convert -size 80x60 %s %s"%(str.replace(fullphotoname,' ','\ '),str.replace(sname,' ','\ '))

        os.system(cmd)

        # add it to the index
        f.write('''<a href="./%s/%s"><img src="./%s/%s" height=10%s ></a>\n'''%(pdir,bname+'.html',sdir,os.path.basename(photo.name),'%'))

        # make the html page
        fp = open(os.path.join(lpdir,bname+'.html'),"w")
        fp.write("""<html>\n<center>\n""")
        if prev != None:
          fp.write('''<a href="%s"> <<< </a>'''%(prev+'.html'))
        fp.write('''<a href="../%s">   index   </a>'''%(os.path.basename(html_name)))
        if next != None:
          fp.write('''<a href="%s"> >>> </a>'''%(next+'.html'))
        fp.write('<br><br>\n')
        fp.write("""<big><big><b>%s</b></big></big>"""%(nickname))
        fp.write('<br><br>\n')
        fp.write('''<img src="../%s" height=100%s >\n'''%(lnname,'%'))
        fp.write('<br><br>\n')

        # label
        if len(photo.label) != 0:
          c_text = parse(photo.label)
          for text in c_text:
            fp.write("""%s<br>\n"""%(text))

          fp.write('<br><br>\n')

        fp.write("""<hr width="100%" size="2"><br>\n""")

        # items
        if len(photo.items) != 0:
          image = Image.open(photo.name)
          draw = ImageDraw.Draw(image)
          for item in photo.items:
            x = self.font.getsize(item['text'])
            print(x)
            # -18 ? pas compris
            draw.text((item['x']-x[0]/2,item['y']-x[1]/2-18),item['text'],fill=250,font=self.font)

          lnname = os.path.join(lidir,'l_'+photo.name)
          image.save(lnname)

          fp.write('<br><br>\n')
          fp.write('''<img src="../%s" height=100%s >\n'''%(lnname,'%'))
          fp.write('<br><br>\n')




          # header
          if len(photo.header) != 0:
            fp.write("""<div align="left">\n""")
            c_text = parse(photo.header)
            for text in c_text:
              fp.write("""%s<br>\n"""%(text))
            fp.write("""</div>\n""")

          fp.write("""</center>\n</html>\n""")
          fp.close()

      f.write("""</center>\n<hr><p>\n</html>\n""")
      f.write("""<a href="../index.html">back</a>\n""")
      f.write("""</html>\n""")
      f.close()


  ##########################
  def WriteNewHtml(self):
  ##########################

    subimage_height = 1080


    html_name = os.path.join(self.dir,'index.html')

    # small dir
    sdir =      'subimages'
    idir =      'images'

    # long dir
    lsdir =     os.path.join(self.dir,'subimages')
    lidir =     os.path.join(self.dir,'images')



    savename =  os.path.basename(html_name)

    if os.path.exists(lidir):
      shutil.rmtree(lidir)
    if os.path.exists(lsdir):
      shutil.rmtree(lsdir)

    os.mkdir(lsdir)
    os.mkdir(lidir)

    # open the html files


    f1 = open('slider.html','w')
    f1.write(flex_html_header)


    f2 = open('index.html','w')
    f2.write(hoverbox_html_header(self.name))





    # loop over all photos
    for photo in self.photos:

      fullphotoname = os.path.join(self.dir,os.path.basename(photo.name))
      angle = photo.params.get('display','angle')

      ################
      # rotate image
      ################

      '''

      if angle != 0:
        lnname = os.path.join(lidir,os.path.basename(photo.name))
        os.system("convert -rotate %i %s %s"%(-angle,str.replace(fullphotoname,' ','\ '),str.replace(lnname,' ','\ ')))
        print "creating (1) :",str.replace(fullphotoname,' ','\ '),str.replace(lnname,' ','\ ')
        lnname = os.path.join(idir,os.path.basename(photo.name))
      else:
        lnname = os.path.basename(photo.name)

      '''


      ################
      # make sub image
      ################


      sname = os.path.join(lsdir,os.path.basename(photo.name))

      image = Image.open(photo.name)

      if angle != 0:

        redfact = float(subimage_height)/image.size[1]
        size0= int(image.size[0]*redfact),int(image.size[1]*redfact)

        image = image.rotate(angle)


        redfact = float(subimage_height)/image.size[1]
        size= int(image.size[0]*redfact),int(image.size[1]*redfact)
        image = image.resize(size)

        imgbg  = Image.new('RGB', (size0[0],size0[1]), (0,0,0))

        x  = int( (size0[0] - size[0])/2. )
        y  = 0
        dx = image.size[0]
        dy = image.size[1]
        box = (x, y, x+dx, y+dy)
        imgbg.paste(image, box)

        image = imgbg

      else:

        redfact = float(subimage_height)/image.size[1]
        size= int(image.size[0]*redfact),int(image.size[1]*redfact)
        image = image.resize(size)






      image.save(sname)

      print(("creating (2) :",str.replace(fullphotoname,' ','\ '),str.replace(sname,' ','\ ')))




      # write html files

      f1.write( ' <li><img src="%s"  /></li> \n '%(sname)  )

      sname = os.path.join( "Icons", os.path.basename(photo.name) )

      f2.write(hoverbox_html_image_link(sname))


    f1.write(flex_html_footer)
    f2.write(hoverbox_html_footer())







  ##########################
  def UpdateAllAlbumNameUsingDates(self):
  ##########################
    '''
    change the album name in each photo using date
    '''

    i = 0
    date = None
    stodate = None

    for photo in self.photos:

      date = photo.GetDate()
      if date!=stodate:
        i = i + 1


      name = "%s_%03d"%(self.name,i)

      photo.params.set('display','album',name)

      stodate = date

    # set global album name
    if self.name == None:
      self.name = self.photos[0].params.get('display','album')




  ##########################
  def UpdateAllAlbumName(self):
  ##########################
    '''
    change the album name in each photo
    '''
    for photo in self.photos:
      photo.params.set('display','album',self.name)


  ##########################
  def UpdateAllNextAlbumName(self):
  ##########################
    '''
    change the album name in each photo after the current ones
    '''

    #name = self.GetCurrent().params.get('display','album')
    name = self.GetMaster().params.get('display','album')

    photolist = self.photos[self.i:]

    for photo in photolist:
      photo.params.set('display','album',name)







##########################
class Photo:
##########################

  def __init__(self,photo_name=None,directory=None,number=None,album=None,video=None):

    basename = os.path.basename(photo_name)
    display = {'angle':      ['f',0],
               'x' :         ['i',0],
               'y' :         ['i',0],
               'size' :      ['f',1.],
               'nickname' :  ['s',basename],
               'album':      ['s',None]}

    params = {'display':display}

    self.params = param.Params(params)			# create the object param

    self.number     = number
    self.name       = photo_name    # this is the complete path
    self.header     = []
    self.label      = []
    self.items      = []
    self.tags       = None
    self.directory  = directory
    
    if video is not None:
      print("before :",video)
      self.video  = os.path.join(os.path.basename(directory),os.path.basename(video))  # related video
    else:
      self.video  = None


  def GetHeader(self):
    return self.header


  def GetDate(self):
    if self.tags!=None:

      if "DateTime" in self.tags:
        date = self.tags["DateTime"]
        "2008:08:05"
        yy = date[0:4]
        mm = date[5:7]
        dd = date[8:10]
        date = yy+mm+dd
        return date


    date = None

    if len(self.header) != 0:
      lines = str.split(self.header,'\n')
      for line in lines:
        sp = str.split(line,':')
        if str.strip(sp[0]) == 'DATE':
          dd = sp[1][1:3]
          mm = sp[1][4:6]
          yy = sp[1][7:11]
          date = yy+mm+dd
          return date

    return date



  def SetAngle(self,angle):
    self.add_angle(angle)

  def GetAngle(self):
    return self.params.get('display','angle')

  def GetAlbum(self):
    return self.params.get('display','album')



  def SetThumbnail(self,thumbnail):
    self.thumbnail = thumbnail

  def GetThumbnail(self):
    return self.thumbnail

  def GetVideo(self):
    return self.video

  def add_label(self,label):
    self.label = label

  def add_header(self,header):
    self.header = header

  def add_video(self,video):
    self.video = video

  def add_tags(self,tags):
    self.tags = tags

  def add_item(self,item):
    self.items.append(item)

  def add_album(self,album):
    self.params.set('display','album',album)

  def add_nickname(self,nickname):
    self.params.set('display','nickname',nickname)

  def add_angle(self,angle):
    self.params.set('display','angle',angle)

  def add_size(self,size):
    self.params.set('display','size',size)

  def add_x(self,x):
    self.params.set('display','x',x)

  def add_y(self,y):
    self.params.set('display','y',y)

  def edit(self):
    entry_label =  editPhotoParam.EditParam(self)

  def create_item(self,x,y):
    itemform = editPhotoParam.ItemForm(self,{'x':x,'y':y,'text':None})

  def edit_item(self,item):
    itemform = editPhotoParam.ItemForm(self,item)

  def print_info(self):
    print("---------------------")
    print((self.name))
    print("---------------------")
    print()

    for l in self.label:
      print(l)

    print()

    for h in self.header:
      print(h)










