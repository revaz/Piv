

class Params:


  def __init__(self,params):

    self.params = params



  ###########################
  def set(self,family,name,value):
  ###########################

    if family in self.params:
      if name in self.params[family]:

        tpe = self.params[family][name][0]

        if   tpe == 'f':
          self.params[family][name][1] = float(value)
        elif tpe == 'i':
          self.params[family][name][1] = int(value)
        elif tpe == 's':
          self.params[family][name][1] = value

      else:
        print((name," does not exit in family ",family))
    else:
      print(("family ",family," does not exit."))


  ###########################
  def get(self,family,name):
  ###########################

    if family in self.params:
      if name in self.params[family]:

        return(self.params[family][name][1])

  ###########################
  def gettype(self,family,name):
  ###########################

    if family in self.params:
      if name in self.params[family]:

        return(self.params[family][name][0])


  ###########################
  def read(self,fname):
  ###########################

    f=open(fname,'r')
    read = 1

    while read:
      line = f.readline()

      if line !='':
        if line !='\n' :
          line = str.strip(line)

          param_family = line[:str.find(line,'.')]
          line = line[str.find(line,'.')+1:]

          param_name   = str.strip(line[:str.find(line,'=')])
          param_value  = str.strip(line[str.find(line,'=')+1:])


          self.set(param_family,param_name,param_value)

      else:
        read = 0                # eof

    f.close()

  ###########################
  def write(self,fname):
  ###########################

    f=open(fname,'w')

    families = list(self.params.keys())
    for family in families:

      names = list(self.params[family].keys())
      for name in names:
        #line = family+"."+name+" = "+`self.params[family][name][1]`+"\n"
        line = family+"."+name+" = "+str(self.params[family][name][1])+"\n"
        f.write(line)

      f.write("\n")

    f.close()


  ###########################
  def list(self):
  ###########################

    lst = []

    families = list(self.params.keys())
    for family in families:

      names = list(self.params[family].keys())
      for name in names:

        lst.append((family,name))

    return lst

  ###########################
  def list_all(self):
  ###########################

    lst = []

    families = list(self.params.keys())
    for family in families:
      names = list(self.params[family].keys())
      for name in names:
        value = self.get(family,name)
        lst.append([family,name,value])

    return lst



  ###########################
  def edit(self):
  ###########################

    import form

    form = form.ParamForm(self)





