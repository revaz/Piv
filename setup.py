#!/usr/bin/env python
'''
 @package   piv
 @file      setup.py
 @brief     Install piv
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of piv.
'''

from setuptools import setup, find_packages
from glob import glob


# scripts to install
scripts = glob("scripts/*")

setup(
    name="Piv",
    author="Yves Revaz",
    author_email="yves.revaz@epfl.ch",
    url="http://obswww.unige.ch/~revaz",
    description="""Piv module""",
    license="GPLv3",
    version="5.0",

    packages=find_packages(),
    scripts=scripts,
    install_requires=[],      # numpy ? pyQt4, Image
)
